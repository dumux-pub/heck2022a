This file has been created automatically. Please adapt it to your needs.

## Content

This contains the test case for coupled stokes darcy flow as found in: Coupling soil/atmosphere interactions and geochemical processes: A multiphase and multicomponent reactive transport approach.

All headers that are required to build the are available in this repository and the versions below. You can configure the module just like any other DUNE
module by using `dunecontrol`. For building and running the executables,
please go to the build folders corresponding to the sources listed above.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|      dune-common      |  origin/releases/2.8  |  fb179f9420efb44ba76bd6219823e436ef63122a  |  2021-09-24 05:38:46 +0000  |
|     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
|       dune-grid       |  origin/releases/2.8  |  e3371946f18df31d4ad542e5e7b51f652954edbc  |  2021-10-26 11:13:47 +0000  |
|  dune-localfunctions  |  origin/releases/2.8  |  f6628171b2773065ab43f97a77f47cd8c4283d8f  |  2021-08-31 14:03:38 +0000  |
|       dune-istl       |  origin/releases/2.8  |  fffb544a61d2c65a0d2fc7c751f36909f06be8f5  |  2021-08-31 13:58:37 +0000  |
|         dumux         |     origin/master     |  37f51626de647161eccade1e6e1776695cbe97ce  |  2022-07-28 00:51:56 +0000  |
|     dune-foamgrid     |     origin/master     |  6632d9c28983fc3936bc471a72d6f9a9a7a213f0  |  2021-12-30 17:13:33 +0000  |
|      dune-subgrid     |  origin/releases/2.8  |  2f8a21a7df84fe01eedb5680868f68c8356d6bd9  |  2021-09-21 12:26:46 +0200  |
|      dune-alugrid     |  origin/releases/2.8  |  ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f  |  2021-08-22 11:59:22 +0200  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_heck2022a.py`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/heck2022a.git heck2022a
./heck2022a/install_heck2022a.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

