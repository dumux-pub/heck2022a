#!/usr/bin/env python3

# 
# This installs the module heck2022a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-common      |  origin/releases/2.8  |  fb179f9420efb44ba76bd6219823e436ef63122a  |  2021-09-24 05:38:46 +0000  |
# |     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
# |       dune-grid       |  origin/releases/2.8  |  e3371946f18df31d4ad542e5e7b51f652954edbc  |  2021-10-26 11:13:47 +0000  |
# |  dune-localfunctions  |  origin/releases/2.8  |  f6628171b2773065ab43f97a77f47cd8c4283d8f  |  2021-08-31 14:03:38 +0000  |
# |       dune-istl       |  origin/releases/2.8  |  fffb544a61d2c65a0d2fc7c751f36909f06be8f5  |  2021-08-31 13:58:37 +0000  |
# |         dumux         |     origin/master     |  37f51626de647161eccade1e6e1776695cbe97ce  |  2022-07-28 00:51:56 +0000  |
# |     dune-foamgrid     |     origin/master     |  6632d9c28983fc3936bc471a72d6f9a9a7a213f0  |  2021-12-30 17:13:33 +0000  |
# |      dune-subgrid     |  origin/releases/2.8  |  2f8a21a7df84fe01eedb5680868f68c8356d6bd9  |  2021-09-21 12:26:46 +0200  |
# |      dune-alugrid     |  origin/releases/2.8  |  ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f  |  2021-08-22 11:59:22 +0200  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.8", "fb179f9420efb44ba76bd6219823e436ef63122a", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.8", "e7bfb66e48496aa28e47974c33ea9a4579bf723b", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.8", "e3371946f18df31d4ad542e5e7b51f652954edbc", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.8", "f6628171b2773065ab43f97a77f47cd8c4283d8f", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.8", "fffb544a61d2c65a0d2fc7c751f36909f06be8f5", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/master", "37f51626de647161eccade1e6e1776695cbe97ce", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "6632d9c28983fc3936bc471a72d6f9a9a7a213f0", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid", "origin/releases/2.8", "2f8a21a7df84fe01eedb5680868f68c8356d6bd9", )

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid", "origin/releases/2.8", "ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f", )

print("Applying patch for uncommitted changes in dumux")
patch = """
diff --git a/dumux/porousmediumflow/nonisothermal/volumevariables.hh b/dumux/porousmediumflow/nonisothermal/volumevariables.hh
index b00ab62b6..cce37f237 100644
--- a/dumux/porousmediumflow/nonisothermal/volumevariables.hh
+++ b/dumux/porousmediumflow/nonisothermal/volumevariables.hh
@@ -326,7 +326,7 @@ public:
     template< bool enable = fullThermalEquilibrium,
               std::enable_if_t<enable, int> = 0>
     Scalar effectiveThermalConductivity() const
-    { return lambdaEff_[0]; }
+    { return 2.2; }
 
     /*!
      * \\brief Returns the effective thermal conductivity \\f$\\mathrm{[W/(m*K)]}\\f$ of the fluids in
"""
applyPatch("dumux", patch)

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=heck2022a/cmake.opts', 'all'],
    '.'
)
